﻿using System;
using Xamarin.Forms;

namespace Echoing
{
	public class BaseMasterDetailPage : MasterDetailPage
	{
		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			if (!App.Instance.IsLoggedIn) {
				Navigation.PushModalAsync(new LoginPage());
			}
		}
	}
}

