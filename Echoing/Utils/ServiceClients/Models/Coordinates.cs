﻿using System;
using Newtonsoft.Json;

namespace Echoing.ServiceClients.Models
{
	public class Coordinates
	{
		[JsonProperty(PropertyName = "lat")]
		public double? Latitude { get; set; }
		[JsonProperty(PropertyName = "lon")]
		public double? Longitude { get; set; }
	}
}

