﻿using System;
using Newtonsoft.Json.Converters;

namespace Echoing.ServiceClients.Converters
{
	public class CustomDateTimeJsonConverter : IsoDateTimeConverter
	{
		public CustomDateTimeJsonConverter()
		{
			base.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
		}
	}
}

