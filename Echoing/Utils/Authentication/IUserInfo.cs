﻿using System;

namespace Echoing
{
	public interface IUserInfo
	{
		string Name { get; set; }
		string Email { get; set; }
		string Picture { get; set; }
	}
}

