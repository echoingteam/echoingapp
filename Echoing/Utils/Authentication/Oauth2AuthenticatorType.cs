﻿using System;
using System.Runtime.Serialization;

namespace Echoing.Utils.Authentication
{
	public enum Oauth2AuthenticatorType
	{
		[EnumMember(Value="Facebook")]
		Facebook,
		[EnumMember(Value="Google")]
		Google
	}
}

