﻿using System;
using Echoing.ServiceClients.Models;
using System.Collections.Generic;

namespace Echoing
{
	public class MapperServiceClient
	{
		public static LocalizedMessage Map(IUserInfo userInfo, string message, double? longitude, double? latitude){
			return new LocalizedMessage {
				Coordinates = new Coordinates { Latitude = latitude, Longitude = longitude },
				MessageContent = message,
				User = new User { Email = userInfo.Email, UserId = 1 },
				Timestamp = DateTime.UtcNow,
				Uuid = DateTime.UtcNow.Ticks.ToString()
			};
		}

		public static ViewModel.Message Map(LocalizedMessage localizedMessage){
			return new ViewModel.Message (localizedMessage.User == null?"tochange@test.com":localizedMessage.User.Email, localizedMessage.MessageContent, "http://i.istockimg.com/file_thumbview_approve/37911674/3/stock-illustration-37911674-vector-user-icon.jpg") {
				Latitude = localizedMessage.Coordinates.Latitude,
				Longitude = localizedMessage.Coordinates.Longitude
			};
		}

		public static IList<ViewModel.Message> Map(IList<LocalizedMessage> localizedMessages){
			var messages = new List<ViewModel.Message> ();
			foreach(var localizedMessage in localizedMessages){
				messages.Add (Map (localizedMessage));
			}
			return messages;
		}

		public static UserPosition Map(IUserInfo userInfo, double? longitude, double? latitude){
			return new UserPosition {
				Coordinates = new Coordinates { Latitude = latitude, Longitude = longitude },
				User = new User { Email = userInfo.Email, UserId = 1 }
			};
		}

	}
}

