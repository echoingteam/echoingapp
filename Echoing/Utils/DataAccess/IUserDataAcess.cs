﻿using System;

namespace Echoing
{
	public interface IUserDataAccess
	{
		IUserInfo GetUser();
		void SaveUser(IUserInfo user);
		void DeleteAll();
		string DbPath { get; set; }
	}
}

